package nl.phazebroek.poc.rockstar;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RequiredArgsConstructor
@RestController
@RequestMapping("rockstars")
public class RockstarController {

    private final RockstarRepository rockstarRepository;

    @GetMapping
    public ResponseEntity<List<Rockstar>> getRockstars() {
        return ResponseEntity.ok(rockstarRepository.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<Rockstar> getRockstar(@PathVariable("id") int id) {
        var rockstar = rockstarRepository.findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Rockstar not found"));
        return ResponseEntity.ok(rockstar);
    }

    @PostMapping
    public ResponseEntity<Void> addRockstar(@Valid @RequestBody Rockstar rockstar) {
        rockstar.setId(null);
        rockstarRepository.save(rockstar);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{id}")
    public ResponseEntity<Void> updateRockstar(@PathVariable("id") int id, @Valid @RequestBody Rockstar rockstar) {
        rockstar.setId(id);
        rockstarRepository.save(rockstar);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteRockstar(@PathVariable("id") int id) {
        var rockstar = rockstarRepository.findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Rockstar not found"));
        rockstarRepository.delete(rockstar);
        return ResponseEntity.noContent().build();
    }
}
