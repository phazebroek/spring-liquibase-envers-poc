package nl.phazebroek.poc.rockstar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

@Getter
@Setter
@Entity
@Audited
public class Rockstar {

    @Id
    @GeneratedValue(generator = "rockstar_id_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @NotNull
    @Column(length = 50)
    private String firstname;

    @NotNull
    @Column(length = 50)
    private String lastname;

    @Column(length = 50)
    private String tribe;

}
