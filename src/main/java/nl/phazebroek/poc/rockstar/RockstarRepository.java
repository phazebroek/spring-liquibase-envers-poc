package nl.phazebroek.poc.rockstar;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RockstarRepository extends JpaRepository<Rockstar, Integer> {

}
